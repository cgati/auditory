package main

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{
		"Index",
		"GET",
		"/",
		Index,
	},
	Route{
		"APIPathList",
		"GET",
		"/api/music",
		ArtistList,
	},
	Route{
		"AlbumList",
		"GET",
		"/api/music/{artistName}",
		AlbumList,
	},
	Route{
		"TrackList",
		"GET",
		"/api/music/{artistName}/{albumName}",
		TrackList,
	},
}
