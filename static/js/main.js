var request = new XMLHttpRequest();
request.open('GET', '/api/music', true);

request.onload = function() {
    if (this.status >= 200 && this.status < 400) {
        var data = JSON.parse(this.response);
        var list = document.querySelector('.list-group');
        data.forEach(function(item, i) {
            var list_item = document.createElement("a");
            list_item.text = item.name;
            list_item.className = "list-group-item";
            list_item.href = "#";
            if (item.dir) {
                list_item.addEventListener("click", function() {
                    getAlbumList(list_item.text);
                }, false);
            }
            list.appendChild(list_item);
        });
    } else {
        // Response Error
    }
};

request.onerror = function() {
    // Connection error.
};

request.send();

function getAlbumList(artist) {
    var request = new XMLHttpRequest();
    request.open('GET', '/api/music/' + artist, true);

    request.onload = function() {
        if (this.status >= 200 && this.status < 400) {
            var data = JSON.parse(this.response);
            var list = document.querySelector('.list-group');
            list.innerHTML = '';
            data.forEach(function(item, i) {
                var list_item = document.createElement("a");
                list_item.text = item.name;
                list_item.className = "list-group-item";
                list_item.href = "#";
                if (item.dir) {
                    list_item.addEventListener("click", function() {
                        getTrackList(artist, list_item.text);
                    }, false);
                }
                list.appendChild(list_item);
            });
        } else {
            // Response Error
        }
    };

    request.onerror = function() {
        // Connection error.
    };

    request.send();
}

function getTrackList(artist, album) {
    var request = new XMLHttpRequest();
    request.open('GET', '/api/music/' + artist + '/' + album, true);

    request.onload = function() {
        if (this.status >= 200 && this.status < 400) {
            var data = JSON.parse(this.response);
            var list = document.querySelector('.list-group');
            list.innerHTML = '';
            data.forEach(function(item, i) {
                var list_item = document.createElement("a");
                list_item.className = "list-group-item";
                list_item.href = "#";
                
                var text = document.createTextNode(" " + item.name);
                list_item.appendChild(text);
                
                var icon = document.createElement("i");
                icon.className = "fa fa-play-circle-o fa-lg";
                
                var audio = document.createElement("audio");
                audio.setAttribute("preload", "none");
                audio.src = item.url;
                
                list_item.addEventListener("click", function() {
                    var audio = this.querySelector('audio');
                    if (audio.paused) { audio.play(); } else { audio.pause(); }
                    // var icon = document.createElement("i");
                    // icon.className = "fa fa-pause-circle-o fa-lg";
                    // this.text = '';
                    
                    // var text = document.createTextNode(" " + item.name);
                    // this.appendChild(text);
                    
                    // this.insertBefore(icon, text);
                }, false);
                
                list_item.insertBefore(icon, text);
                list_item.appendChild(audio);
                
                list.appendChild(list_item);
            });
        } else {
            // Response Error
        }
    };

    request.onerror = function() {
        // Connection error.
    };

    request.send();
}
