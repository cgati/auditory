package main

import (
	"log"
	"net/http"
)

func main() {

	r := NewRouter()

	s := http.FileServer(http.Dir("./static/"))
	m := http.FileServer(http.Dir("/media/cgati/blackbox/Music/"))
	http.Handle("/static/", http.StripPrefix("/static/", s))
	http.Handle("/media/", http.StripPrefix("/media/", m))

	http.Handle("/", r)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
