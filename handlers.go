package main

import (
	"encoding/json"
	// "fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	"github.com/gorilla/mux"
)

// Path is a structure representing the music library on disk
type Path struct {
	Name string `json:"name"`
	URL  string `json:"url"`
	Dir  bool   `json:"dir"`
}

// Paths is a list of Path objects on disk
type Paths []Path

// Index is an HTTP Handler for the Index page
func Index(w http.ResponseWriter, r *http.Request) {
	renderTemplate(w, "main")
}

// ArtistList is an HTTP Handler for the Artists in the music library
func ArtistList(w http.ResponseWriter, r *http.Request) {
	dir := "/media/cgati/blackbox/Music/"
	files, _ := ioutil.ReadDir(dir)
	paths := Paths{}
	for _, f := range files {
		loc := ""
		if !f.IsDir() {
			u, err := url.Parse("/media/" + f.Name())
			if err != nil {
				loc = ""
			}
			loc = u.String()
		}
		paths = append(paths, Path{
			Name: f.Name(),
			Dir:  f.IsDir(),
			URL: loc,
		})
	}

	w.Header().Set("Content-Type", "application/json;charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	if err := json.NewEncoder(w).Encode(paths); err != nil {
		panic(err)
	}
}

// AlbumList is an HTTP Handler for the Albums for a particular Artist
func AlbumList(w http.ResponseWriter, r *http.Request) {
	dir := "/media/cgati/blackbox/Music/"
	vars := mux.Vars(r)
	artistName := vars["artistName"]
	files, _ := ioutil.ReadDir(dir + artistName)
	paths := Paths{}
	for _, f := range files {
		loc := ""
		if !f.IsDir() {
			u, err := url.Parse("/media/" + artistName + "/" + f.Name())
			if err != nil {
				loc = ""
			}
			loc = u.String()
		}
		paths = append(paths, Path{
			Name: f.Name(),
			Dir:  f.IsDir(),
			URL: loc,
		})
	}

	w.Header().Set("Content-Type", "application/json;charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	if err := json.NewEncoder(w).Encode(paths); err != nil {
		panic(err)
	}
}

// TrackList is an HTTP Handler for the Tracks for a particular Album
func TrackList(w http.ResponseWriter, r *http.Request) {
	dir := "/media/cgati/blackbox/Music/"
	vars := mux.Vars(r)
	artistName := vars["artistName"]
	albumName := vars["albumName"]
	files, _ := ioutil.ReadDir(dir + artistName + "/" + albumName)
	paths := Paths{}
	for _, f := range files {
		loc := ""
		if !f.IsDir() {
			u, err := url.Parse("/media/" + artistName + "/" + albumName + "/" + f.Name())
			if err != nil {
				loc = ""
			}
			loc = u.String()
		}
		paths = append(paths, Path{
			Name: f.Name(),
			Dir:  f.IsDir(),
			URL: loc,
		})
	}

	w.Header().Set("Content-Type", "application/json;charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	if err := json.NewEncoder(w).Encode(paths); err != nil {
		panic(err)
	}
}
